package app.model.response;

import java.util.List;

import app.model.Consulta;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ConsultaResponse {
    private List<Consulta> consulta;
    private InfoPage infoPagina;
}
