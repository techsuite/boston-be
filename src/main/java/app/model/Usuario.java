package app.model;

import lombok.*;
import java.sql.Date;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Usuario{

  private String dni;
  private String nombre;
  private String apellidos;
  private String email;
  private String contrasena;
  private Date fechaDeNacimiento;
  private String genero;
}