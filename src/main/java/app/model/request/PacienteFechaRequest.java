package app.model.request;

import java.sql.Date;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class PacienteFechaRequest{

    private String dniPaciente;
    private Date fecha;

}
