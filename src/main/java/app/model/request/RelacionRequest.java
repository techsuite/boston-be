package app.model.request;
import app.model.Paciente;
import app.model.Usuario;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class RelacionRequest{

    private String dniUsuario;
    private String dniPaciente;

}