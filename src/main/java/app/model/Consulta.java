package app.model;

import java.sql.Date;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Consulta {
    private int idConsulta;
    private Date fechaRegistroPaciente, fechaConsulta;

    private String enfermedadesDiagnosticadas, medicamentosRecetados, pruebasDeLaboratorioARealizar, observaciones;

    private String dniDoctor, nombreDoctor, apellidosDoctor;
    private Date fechaNacimientoDoctor;
    private String correoElectronicoDoctor, generoDoctor, passwordDoctor;

    private String dniPaciente, nombrePaciente, apellidosPaciente;
    private Date fechaNacimientoPaciente;
    private String correoElectronicoPaciente, generoPaciente;
}