package app.model;

import lombok.*;
import java.sql.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode

public class Paciente{
  private String dni;
  private String nombre;
  private String apellido;
  private String email;
  private Date fechaRegistro;
  private Date fechaUltimaConsulta;
  private Date fechaNacimiento;
  private String genero;
}