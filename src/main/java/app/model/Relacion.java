package app.model;

import lombok.*;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Relacion {
  private int id; 
  private String dniDoctor;
  private String dniPaciente;
}
