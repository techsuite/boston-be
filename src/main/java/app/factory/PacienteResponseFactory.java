package app.factory;

import app.model.Paciente;
import app.model.response.PacienteResponse;
import app.model.response.InfoPage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PacienteResponseFactory {

    public PacienteResponse newPacienteResponse(List<Paciente> pacientes, InfoPage infoPage){
        return new PacienteResponse(pacientes, infoPage);
    }
}
