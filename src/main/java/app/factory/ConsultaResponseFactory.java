package app.factory;

import app.model.Consulta;
import app.model.response.ConsultaResponse;
import app.model.response.InfoPage;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ConsultaResponseFactory {

    public ConsultaResponse newConsultaResponse(List<Consulta> consultas, InfoPage infoPage){
        return new ConsultaResponse(consultas, infoPage);
    }
}
