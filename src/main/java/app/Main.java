package app;

import java.sql.Date;
import java.sql.SQLException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import app.dao.ConsultaDao;
import app.dao.PacienteDao;
import app.model.Consulta;

@SpringBootApplication
public class Main {

  public static void main(String[] args) throws SQLException {
    SpringApplication.run(Main.class, args);
  }
}
