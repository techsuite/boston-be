package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import java.util.List;
import java.util.ArrayList;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.model.response.InfoPage;

import app.dao.connector.Connector;
import app.model.Paciente;

@Repository
public class PacienteDao extends BaseDao {

    
    public static final String nombreTabla = "boston.Paciente";

    static final String obtenerPacientes = "SELECT * FROM " + nombreTabla + ";";
    static final String obtenerPacienteSQL = "SELECT * FROM " + nombreTabla + " WHERE DNI = ?";
    static final String insertarPacienteSQL = "INSERT INTO " + nombreTabla + " VALUES (?, ?, ?, ?, ?, ?, ?, ?) ;";
    static final String actualizarFechaPacienteSQL = "UPDATE " + nombreTabla + " SET fecha_ult_consulta = ? WHERE DNI = ? ;";

    public PacienteDao() {
      setVars(nombreTabla);
    }

    public Paciente getPacienteByDNI(String dni) throws SQLException {
        Paciente paciente = null;
        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(obtenerPacienteSQL);
            ){
            pst.setString(1, dni);
            try(ResultSet rs = pst.executeQuery()){
                if(rs.next()){
                    paciente = new Paciente(
                        rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
                        rs.getDate(5),rs.getDate(6),rs.getDate(7),
                        rs.getString(8)
                    );
                }
            }
        }

        return paciente;
    }


    public List<Paciente> getPacientesPagesFromQuery(InfoPage info) throws SQLException {
        List<Paciente> pacientes = new ArrayList<Paciente>();
        String sqlQuery = "SELECT * FROM " + tabla
        +" " + paginationQuery(info);

        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(sqlQuery);
            ){
            //pst.setString(1, dni);
            try(ResultSet rs = pst.executeQuery()){
                while (rs.next()){
                    Paciente paciente = new Paciente(
                        rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
                        rs.getDate(5),rs.getDate(6),rs.getDate(7),
                        rs.getString(8)
                    );
                    pacientes.add(paciente);
                }
            }
        }

        return pacientes;
    }



    //Si lanza una excepcion es que no se ha podido insertar el paciente
    //Si la excepcion es del tipo SQLIntegrityConstraintViolationException
    // es que ya existe alguien con la misma pk (dni)
    public void insertarPaciente(Paciente paciente) throws SQLException{
        try(Connection conn = connector.getConnection();
          PreparedStatement pst = conn.prepareStatement(insertarPacienteSQL);
          ){
          //toLowerCase para asegurarnos de que es la misma pk
          pst.setString(1, paciente.getDni().toLowerCase());

          pst.setString(2, paciente.getNombre());
          pst.setString(3, paciente.getApellido());
          pst.setString(4, paciente.getEmail());
          pst.setDate(5, paciente.getFechaRegistro());
          pst.setDate(6, paciente.getFechaUltimaConsulta());
          pst.setDate(7, paciente.getFechaNacimiento());
          pst.setString(8, paciente.getGenero());

          pst.executeUpdate();
          pst.close();
        }
    }

    //Devuelve true si se ha actualizado alguna fila
    public boolean actualizarFecha(String dni, Date fecha) throws SQLException {
      int rowsUpdated = 0;
      try(Connection conn = connector.getConnection();
          PreparedStatement pst = conn.prepareStatement(actualizarFechaPacienteSQL);
          ){
          pst.setDate(1, fecha);
          pst.setString(2, dni.toLowerCase());
          rowsUpdated = pst.executeUpdate();
          pst.close();
        }

      return rowsUpdated > 0;
    }
}
