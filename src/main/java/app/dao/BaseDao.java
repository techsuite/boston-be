package app.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.GenerousBeanProcessor;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.connector.Connector;
import app.model.response.InfoPage;

@Repository
public class BaseDao {

    @Autowired
    protected Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    protected String tabla;

    public void setVars(String tabla) {
        this.tabla = tabla;
        this.connector = new Connector();
        this.queryRunner = new QueryRunner();
    }

    protected String paginationQuery(InfoPage info) {
        return String.format(
        "LIMIT %d OFFSET %d",
        info.getPageSize(),
        (info.getPageSize() * (info.getPageNumber() - 1))
        );
    }

    public <T> List<T> getPagesFromQuery(
        String query,
        InfoPage info,
        Class<T> tipo
        ) throws Exception {
        String finalQuery = String.format("%s %s", query, paginationQuery(info));
        try (Connection conn = connector.getConnection()) {
          return queryRunner.query(
            conn,
            finalQuery,
            new BeanListHandler<>(
              tipo,
              // Comentario de Julio: esto hace que DBUtils ignore underscores y mayúsculas a la hora de mapear
              // las columnas con el bean (model). He visto que no funciona al completo pq en la base de datos tenéis
              // atributos de la forma "enfermedades_diag" y en el model "enfermedadesDiagnosticadas". Y claro,
              // no consigue mapear correctamente las columnas
              new BasicRowProcessor(new GenerousBeanProcessor())
            )
          );
        }
    }

    public Long numOfRowsFromQuery(String query) throws Exception {
        String finalQuery = String.format(
          "SELECT COUNT(*) FROM %s ;",
          query
        );
        try (Connection conn = connector.getConnection()) {
           return queryRunner.query(conn, finalQuery, new ScalarHandler<Long>());
        }
    }

    public Long numOfRows() throws Exception {
        return numOfRowsFromQuery(tabla);
    }

    public Long numOfPages(Long pageSize, Long rowsInTable) throws Exception {
        if (rowsInTable % pageSize == 0)
            return rowsInTable / pageSize;
        else
            return rowsInTable / pageSize + 1;
    }

    public InfoPage getMetadataFromQuery(InfoPage info, String query) throws Exception {
        Long numPages = numOfPages(new Long(info.getPageSize()), numOfRowsFromQuery(query));
        return info.getPageNumber() <= numPages
        ? new InfoPage(info.getPageSize(), info.getPageNumber(), numPages.intValue())
        : null;
    }

    // null if infoPage > totalPages
    public InfoPage getMetadata(InfoPage info) throws Exception {
        return getMetadataFromQuery(info, tabla);
    }
}
