package app.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.model.response.InfoPage;

import app.dao.connector.Connector;
import app.dao.*;
import app.model.*;

@Repository
public class ConsultaDao extends BaseDao {

    protected UsuarioDao doctorDao;
    protected PacienteDao pacienteDao;

    public static final String nombreTabla = "boston.Consulta";

    static final String obtenerConsulta = "SELECT * FROM "+ nombreTabla +" JOIN boston.Usuario ON DNI_Usuario=boston.Usuario.dni JOIN boston.Paciente ON DNI_paciente = ? AND DNI_paciente=boston.Paciente.DNI  ";

    static final String insertarConsultaSQL = "INSERT INTO " + nombreTabla + " (DNI_usuario, DNI_paciente, fecha_consulta, enfermedades_diag, medicamentos_recetados, pruebas_lab, observacion) VALUES (?, ?, ?, ?, ?, ?, ?) ";
    static final String actualizarConsultaSQL = "UPDATE " + nombreTabla + " SET enfermedades_diag = ?, medicamentos_recetados = ?, pruebas_lab = ?, observación = ? WHERE id_consulta = ? ";
    static final String eliminarConsultaSQL = "DELETE FROM " + nombreTabla + " WHERE id_consulta = ? ";

    public ConsultaDao() {
      doctorDao = new UsuarioDao();
      pacienteDao = new PacienteDao();
      setVars(nombreTabla);
    }

    public List<Consulta> getConsultaByDNI(String dni, InfoPage info) throws SQLException {
        ArrayList<Consulta> ls = new ArrayList<>();
        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(obtenerConsulta + paginationQuery(info));
            ){
            pst.setString(1, dni);
            System.out.println(pst);
            try(ResultSet rs = pst.executeQuery()){
                while (rs.next()){
                    ls.add(new Consulta(rs.getInt(1),rs.getDate(20),rs.getDate(4),rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                          rs.getString(9), rs.getString(10), rs.getString(11), rs.getDate(14), rs.getString(12), rs.getString(15), rs.getString(13),
                          rs.getString(16), rs.getString(17), rs.getString(18), rs.getDate(22), rs.getString(19), rs.getString(23)
                    ));
                }
            }
        }

        return ls;
    }

    public Integer insertarConsulta(Consulta consulta) throws SQLException{
        Integer pk = -1;
        try(Connection conn = connector.getConnection();
          PreparedStatement pst = conn.prepareStatement(insertarConsultaSQL, Statement.RETURN_GENERATED_KEYS);
          ){
          pst.setString(1, consulta.getDniDoctor().toLowerCase());
          pst.setString(2, consulta.getDniPaciente().toLowerCase());

          pst.setDate(3, consulta.getFechaConsulta());
          pst.setString(4, consulta.getEnfermedadesDiagnosticadas());
          pst.setString(5, consulta.getMedicamentosRecetados());
          pst.setString(6, consulta.getPruebasDeLaboratorioARealizar());
          pst.setString(7, consulta.getObservaciones());

          pst.executeUpdate();
          ResultSet rs = pst.getGeneratedKeys();
          if (rs.next()) pk = rs.getInt(1);
          pst.close();
        }

      return pk;
    }

    /*public void actualizarConsulta(Consulta consulta) throws SQLException {
      try(Connection conn = connector.getConnection();
          PreparedStatement pst = conn.prepareStatement(actualizarConsultaSQL);
          ){
          pst.setString(1, consulta.getEnfermedadesDiagnosticadas());
          pst.setString(2, consulta.getMedicamentosRecetados());
          pst.setString(3, consulta.getPruebasDeLaboratorioARealizar());
          pst.setString(4, consulta.getObservaciones());

          pst.setInt(5, consulta.getIdConsulta());

          pst.executeUpdate();
          pst.close();
        }
    }*/

    public List<Consulta> getConsultaPagesFromQuery(InfoPage info) throws SQLException {
        List<Consulta> ls = new ArrayList<Consulta>();
        String sqlQuery = "SELECT * FROM boston.Consulta JOIN boston.Usuario ON DNI_Usuario=boston.Usuario.dni JOIN boston.Paciente ON DNI_paciente=boston.Paciente.DNI  "
        + paginationQuery(info);

        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(sqlQuery);
            ){
            //pst.setString(1, dni);
            try(ResultSet rs = pst.executeQuery()){
                while (rs.next()){
                    ls.add(new Consulta(rs.getInt(1),rs.getDate(20),rs.getDate(4),rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8),
                    rs.getString(9), rs.getString(10), rs.getString(11), rs.getDate(14), rs.getString(12), rs.getString(15), rs.getString(13),
                    rs.getString(16), rs.getString(17), rs.getString(18), rs.getDate(22), rs.getString(19), rs.getString(23)
                    ));
                }
            }
        }

        return ls;
    }

    public String getIdPacienteByIdConsulta(int id) throws SQLException{
      String sql = "Select * from boston.Consulta where id_consulta = ?";
      String res = null;

      try( Connection conn = connector.getConnection();
      PreparedStatement pst = conn.prepareStatement(sql)){
        pst.setInt(1, id);
        try(ResultSet rs = pst.executeQuery()){
          if(rs.next())
            res = rs.getString(3);
        }
      }
      return res;
    }

    public Date getNuevaFechaConsulta(String idPaciente)throws SQLException{
      String sql = "Select * from boston.Consulta where DNI_paciente = ?";
      Date newDate = null;
      if(getNumConsultasPaciente(idPaciente)>0){
        try(Connection conn = connector.getConnection();
        PreparedStatement pst = conn.prepareStatement(sql)){
          pst.setString(1, idPaciente);
          try(ResultSet rs = pst.executeQuery()){
            while(rs.next())  
              newDate = rs.getDate("fecha_consulta");
          }
        }
      }
      return newDate;
    }

    public Integer getNumConsultasPaciente(String idPaciente) throws SQLException{
      String sql = "SELECT count(*) FROM boston.Consulta where DNI_paciente = ? ;";
      Integer res = -1;
      try(Connection conn = connector.getConnection();
      PreparedStatement pst = conn.prepareStatement(sql)){
        pst.setString(1, idPaciente);
        try(ResultSet rs = pst.executeQuery()){
          if(rs.next())  
            res = rs.getInt(1);
        }
      }
      return res;
    }

    //Devuelve true si ha eliminado alguna Consulta
    public boolean eliminarConsultaByID(Integer id) throws SQLException {
      boolean eliminado = false;
      try(Connection conn = connector.getConnection();
        PreparedStatement pst = conn.prepareStatement(eliminarConsultaSQL);
        ){
        pst.setInt(1, id);
        eliminado = pst.executeUpdate() == 1;
      }
      return eliminado;
    }

   /* public boolean eliminarYActualizarById(Integer id) throws SQLException{
      boolean res = false;
      String dniPaciente = getIdPacienteByIdConsulta(id);
      if(dniPaciente!=null){
        res = eliminarConsultaByID(id);
        if(!res){
          return res;
        }
        else{        
          pacienteDao.actualizarFecha(dniPaciente,getNuevaFechaConsulta(dniPaciente));
          return res;
        }
      }
      return res;
    }*/

}
