package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import app.dao.connector.Connector;
import app.model.Usuario;

@Repository
public class UsuarioDao extends BaseDao {

    public static final String nombreTabla = "boston.Usuario";
    static final String sql = "SELECT * FROM " + nombreTabla + " WHERE dni = ?";

    public UsuarioDao(){
        setVars(nombreTabla);
    }

    public Usuario getUsuarioByDNI(String dni) throws SQLException {
        Usuario usuario = null;
        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(sql);
            ){
            pst.setString(1, dni);
            try(ResultSet rs = pst.executeQuery()){
                if(rs.next()){
                    usuario = new Usuario(
                        rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),
                        rs.getDate(6),
                        rs.getString(7)
                    );
                }
            }
        }

        return usuario;
    }

    public Usuario getUsuarioByDNI2(String dni) throws SQLException{
        Usuario usuario = null;
        try(Connection conn = connector.getConnection();){
            usuario = queryRunner.query(conn, sql, new BeanHandler<>(Usuario.class), dni);
        }

        return usuario;
    }

    public boolean verificacionCredendiales(Usuario usuario) throws SQLException {
        Usuario usuarioBBDD = getUsuarioByDNI(usuario.getDni());
        boolean credencialesCorrectas = false;

        if(usuarioBBDD != null){
            String contrasenaBBDD = usuarioBBDD.getContrasena();
            credencialesCorrectas = contrasenaBBDD.equals(usuario.getContrasena());            
        }

        return credencialesCorrectas;
    }
}
