package app.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import app.dao.connector.Connector;
import app.model.Paciente;
import app.model.Relacion;
import app.model.Usuario;
import app.model.response.InfoPage;



@Repository
public class RelacionDao extends BaseDao{

    @Autowired
    protected  Connector connector;

    @Autowired
    protected QueryRunner queryRunner;

    public RelacionDao(){
        connector = new Connector();

    }


    
    static final String comprobarSiRelacionExiste="SELECT * FROM boston.Relacion WHERE DNI_usuario= ? AND DNI_paciente= ? ";

    static final String insertarRelacion="INSERT INTO boston.Relacion (DNI_usuario,DNI_paciente) VALUES (?,?) ;";



    public List<Paciente> obtenerPacienteAsociadoDNIUsuario(String dni, String dniPaciente, InfoPage infoPage) throws SQLException{
        ArrayList<Paciente> pacientes = new ArrayList<>();
        String obtenerPacienteAsociadoDNIUsuario = "SELECT boston.Paciente.* FROM boston.Relacion INNER JOIN boston.Paciente ON Relacion.DNI_usuario= ? "+
        "AND Relacion.DNI_paciente=Paciente.DNI AND Relacion.DNI_paciente LIKE ? ";
        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(obtenerPacienteAsociadoDNIUsuario+" "+paginationQuery(infoPage));

            ){

            pst.setString(1, dni);
            if(dniPaciente.equals(" "))
                dniPaciente="";
            pst.setString(2, dniPaciente+"%");

            try(ResultSet rs = pst.executeQuery()){
                while (rs.next()){
                    pacientes.add(new Paciente(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
                    rs.getDate(5), rs.getDate(6),rs.getDate(7), 
                    rs.getString(8)));
                }
            }
        }
            return pacientes;
    }

    public List<Paciente> obtenerPacienteNoAsociadoDNIUsuario(String dni, String dniPaciente,InfoPage infoPage) throws SQLException{
        List<Paciente> pacientes = new ArrayList<Paciente>();
        String obtenerPacienteNoAsociadoDNIUsuario = "SELECT boston.Paciente.* FROM boston.Paciente WHERE DNI NOT IN ( SELECT boston.Paciente.DNI FROM boston.Relacion "+
        "INNER JOIN boston.Paciente ON Relacion.DNI_usuario= ?  AND Relacion.DNI_paciente=Paciente.DNI) AND DNI LIKE ? ";
        
        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(obtenerPacienteNoAsociadoDNIUsuario+" "+paginationQuery(infoPage));
            ){
            pst.setString(1, dni);            
            if(dniPaciente.equals(" "))
                dniPaciente="";
            pst.setString(2, dniPaciente+"%");    
            try(ResultSet rs = pst.executeQuery()){
                while (rs.next()){
                    pacientes.add(new Paciente(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
                    rs.getDate(5), rs.getDate(6),rs.getDate(7), 
                    rs.getString(8)));
                }
            }
        }

            return pacientes;

    }


    /*public boolean comprobarSiRelacionExiste(Usuario usuario, Paciente paciente) throws SQLException {
        boolean relacion= true;

        try (Connection conn = connector.getConnection();
            PreparedStatement pst = conn.prepareStatement(comprobarSiRelacionExiste);
            ){
                pst.setString(1, usuario.getDni());
                pst.setString(2, paciente.getDni());
            try(ResultSet rs = pst.executeQuery()){
                if(!rs.isBeforeFirst()){
                    relacion=false;
                }
                
            }
        }   

        return relacion;

    }*/





    //Si lanza excepcion, o ya esta intoducida una relacion con iguales DNI o los 

    //DNI de usuario y paciente son exactamente el mismo

    public void insertarRelacion(String dniUsuario,String dniPaciente) throws SQLException{
        try(Connection conn = connector.getConnection();

          PreparedStatement pst = conn.prepareStatement(insertarRelacion);
          ){
          pst.setString(1, dniUsuario.toLowerCase());
          pst.setString(2, dniPaciente.toLowerCase());

          pst.executeUpdate();
          pst.close();

        }

    }



}
