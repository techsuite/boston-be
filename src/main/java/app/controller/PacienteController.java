package app.controller;


import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import app.dao.PacienteDao;
import app.dao.RelacionDao;
import app.model.Paciente;
import app.model.Usuario;
import app.model.request.PacienteFechaRequest;
import app.model.request.RelacionRequest;
import app.model.response.InfoPage;
import app.model.response.PacienteResponse;



@RestController
public class PacienteController {

    @Autowired
    RelacionDao relacionDAO;

    @Autowired
    PacienteDao pacienteDAO;

   /*GET /getPacientes/{dni}/{dniPaciente}/pagina?pageNumber=1&pageSize=2 desde fuera, se puede enviar  un listado paginado de pacientes
 que están asociados(relacionados) a dicho doctor*/
 @GetMapping("/getPacientes/{dni}/{dniPaciente}/pagina")
 public ResponseEntity<PacienteResponse> getPacientesAsociadosByDNI(@PathVariable String dni, @PathVariable String dniPaciente, @Valid InfoPage infoPage) throws Exception{
     
     if (dni.length() > 9 || dniPaciente.length() > 9 || infoPage == null)
       return ResponseEntity.badRequest().build(); //Para evitar code injection
   InfoPage metadata;
   try {        
     if(dniPaciente.equals(" "))
     dniPaciente="";
     metadata = relacionDAO.getMetadataFromQuery(infoPage, "boston.Relacion WHERE DNI_usuario = '"+dni+
     "' and DNI_paciente LIKE '"+dniPaciente+"%'");
   } catch (Exception e) {
     return ResponseEntity.badRequest().build();
   }

   if (metadata == null)       
     return ResponseEntity.badRequest().build();
   
   return ResponseEntity.ok(
     new PacienteResponse(
       relacionDAO.obtenerPacienteAsociadoDNIUsuario(dni, dniPaciente, metadata),
       metadata
     )
   );
 }


    /* GET /getNoPacientes/{dni}/{dniPaciente}/pagina?pageNumber=1&pageSize=2 desde fuera, se puede enviar  un listado paginado de pacientes
     que no están asociados(relacionados) a dicho doctor */
     @GetMapping("/getNoPacientes/{dni}/{dniPaciente}/pagina")
     public ResponseEntity<PacienteResponse> getPacientesNoAsociadosByDNI(@PathVariable String dni,@PathVariable String dniPaciente, @Valid InfoPage infoPage) throws Exception{
         if (dni.length() > 9 || dniPaciente.length() > 9  || infoPage == null)
         return ResponseEntity.badRequest().build(); //Para evitar code injection
     InfoPage metadata;
     try {
       if(dniPaciente.equals(" "))
         dniPaciente="";
       System.out.println("dniPaciente: "+dniPaciente);
       metadata = relacionDAO.getMetadataFromQuery(infoPage,"boston.Paciente WHERE DNI NOT IN (SELECT boston.Paciente.DNI FROM boston.Relacion INNER JOIN boston.Paciente "+ 
         "ON Relacion.DNI_usuario='"+dni+"' AND Relacion.DNI_paciente=Paciente.DNI) AND DNI LIKE '"+dniPaciente+"%';");
     } catch (Exception e) {
         return ResponseEntity.badRequest().build();
     }
 
     if (metadata == null){      
       return ResponseEntity.badRequest().build();
     }
     return ResponseEntity.ok(
       new PacienteResponse(
         relacionDAO.obtenerPacienteNoAsociadoDNIUsuario(dni,dniPaciente, metadata),
         metadata
       )
     );
    }

    /* GET /pacientes
    Devuelve todos los pacientes */
    @GetMapping("/pacientes")
  public ResponseEntity<PacienteResponse> pacientes(
    InfoPage infoPage)
    throws Exception {
    // pageNumber>0, pageSize>0, pageNumber<totalPages, idioma = español o idioma = ingles.

    if (infoPage == null)
        return ResponseEntity.badRequest().build();

    InfoPage metadata = null;
    try {
        metadata = pacienteDAO.getMetadata(infoPage);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
    if (metadata == null)
      return ResponseEntity.badRequest().build();

    
    return ResponseEntity.ok(
      new PacienteResponse(
       pacienteDAO.getPacientesPagesFromQuery(infoPage), 
        metadata
        )
    );
  }
    
    /* Función de PostMapping /insertarNuevo/{paciente} que permite insertar nuevo paciente en el SQL */ 
    
    @PostMapping("/insertarNuevo")
    public void insertarPaciente(@RequestBody Paciente paciente) throws SQLException {
        pacienteDAO.insertarPaciente(paciente);
    }

    /*Función de PostMapping /asociarPaciente/{usuario}/{paciente} que permite insertar nueva relación en SQL*/
    
    @PostMapping("/asociarPaciente")
    public void insertarRelacion(@RequestBody RelacionRequest relacionRequest) throws SQLException {
        relacionDAO.insertarRelacion(relacionRequest.getDniUsuario(), relacionRequest.getDniPaciente());
    }

    /* Función que PostMapping /actualizarFechaConsulta/{dniPaciente}/{fecha} que permite actualizar la fecha de ultima consulta de un paciente */ 

    @PostMapping("/actualizarFechaConsulta")
    public void insertarPaciente(@RequestBody PacienteFechaRequest pacienteFechaRequest) throws SQLException {
       pacienteDAO.actualizarFecha(pacienteFechaRequest.getDniPaciente(), pacienteFechaRequest.getFecha());
    }

}