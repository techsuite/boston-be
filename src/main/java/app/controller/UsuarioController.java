package app.controller;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import app.dao.UsuarioDao;
import app.model.Usuario;


@RestController
public class UsuarioController {
    
    @Autowired
    UsuarioDao usuarioDAO;

    @GetMapping("/user/{dni}")
    public ResponseEntity<Usuario> getUsuarioByDNI(@PathVariable String dni) throws Exception{
        Usuario usuario=usuarioDAO.getUsuarioByDNI2(dni);

        return usuario!=null 
        ? ResponseEntity.ok().body(usuario) 
        : ResponseEntity.noContent().build(); 
    }

    @GetMapping("/check/{dni}/{password}")
    public ResponseEntity<Boolean> getCheckCredentials(@PathVariable String dni,@PathVariable String password) throws Exception{
        Usuario user=new Usuario(dni,"","","", password,Date.valueOf("1900-01-01"),"");
    
        Boolean res = usuarioDAO.verificacionCredendiales(user);

        return ResponseEntity.ok().body(res);

    }
}
