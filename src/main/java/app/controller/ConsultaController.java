package app.controller;

import app.dao.ConsultaDao;
import app.dao.PacienteDao;
import app.factory.ConsultaResponseFactory;
import app.model.Consulta;
import app.model.response.ConsultaResponse;
import app.model.response.InfoPage;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;

import com.mysql.cj.PerConnectionLRUFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.annotation.ComponentScan;

@RestController
@ComponentScan("app.factory")
public class ConsultaController {

  @Autowired
  private ConsultaResponseFactory consultaResponseFactory;

  @Autowired
  ConsultaDao consultaDAO;
  
  @Autowired
  PacienteDao pacienteDAO;

  // Igual era una mejor idea que esta llamada estuviera contenida en la otra
  // y que el dni llegara como queryParam opcional
  @GetMapping("/consultasDNI/{dniPaciente}/pagina")
  public ResponseEntity<ConsultaResponse> consultasDni(
    @PathVariable String dniPaciente,
    InfoPage infoPage
  ) throws Exception {
    if (dniPaciente.length() > 9 || infoPage == null)
        return ResponseEntity.badRequest().build(); //Para evitar code injection
    
    InfoPage metadata;
    try {
        metadata = consultaDAO.getMetadataFromQuery(infoPage, String.format(" boston.Consulta JOIN boston.Usuario ON DNI_Usuario=boston.Usuario.dni JOIN boston.Paciente ON DNI_paciente='%s' AND DNI_paciente=boston.Paciente.DNI;", dniPaciente));;
      } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
    if (metadata == null)
      return ResponseEntity.badRequest().build();

    return ResponseEntity.ok(
      new ConsultaResponse(
        consultaDAO.getConsultaByDNI(dniPaciente, metadata),
        metadata
      )
    );
    //      // pageNumber>0, pageSize>0, pageNumber<totalPages, idioma = español o idioma = ingles.
    //      Boolean result = infoPage.getPageNumber() > 0 && infoPage.getPageSize() > 0;
    //      List<Consulta> res = new ArrayList<Consulta>();
    //
    //      if (result == true) {
    //        res = consultaDAO.getConsultaByDNI(dniPaciente, infoPage);
    //        Integer totalRows = consultaDAO.numOfRowsWithCommand(
    //          "boston.Consulta WHERE DNI_paciente = " + dniPaciente
    //        );
    //
    //        double div = Math.ceil((double) totalRows / infoPage.getPageSize());
    //        infoPage.setTotalPages((int) div);
    //        if (infoPage.getPageSize() > totalRows) {
    //          result = false;
    //          div = 0.0;
    //          infoPage.setTotalPages((int) div);
    //        }
    //      }
    //      ConsultaResponse consultaResponse = consultaResponseFactory.newConsultaResponse(
    //        res,
    //        infoPage
    //      );
    //
    //      result = result && infoPage.getPageNumber() <= infoPage.getTotalPages();
    //
    //      return result
    //        ? ResponseEntity.ok().body(consultaResponse)
    //        : ResponseEntity.badRequest().build();
  }

  @GetMapping("/consultas")
  public ResponseEntity<ConsultaResponse> consultas(
    InfoPage infoPage)
    throws Exception {
    // pageNumber>0, pageSize>0, pageNumber<totalPages, idioma = español o idioma = ingles.

    if (infoPage == null)
        return ResponseEntity.badRequest().build();

    InfoPage metadata = null;
    try {
        metadata = consultaDAO.getMetadata(infoPage);
    } catch (Exception e) {
      return ResponseEntity.badRequest().build();
    }
    if (metadata == null)
      return ResponseEntity.badRequest().build();

    // Comentario de Julio: No hace falta tener factories de models, de hecho no tiene sentido hacerlo pq
    // los factories normalmente los usamos para luego poder mockear en los tests, y los model NUNCA queremos
    // mockearlos pq representan los datos que manejamos en nuestro código
    return ResponseEntity.ok(
      new ConsultaResponse(
        consultaDAO.getConsultaPagesFromQuery(infoPage), 
        metadata
        )
    );

    //    if (result == true) {
    //      res = consultaDAO.getConsultaPagesFromQuery(infoPage);
    //      Integer totalRows = consultaDAO.numOfRowsWithCommand("boston.Consulta");
    //
    //      double div = Math.ceil((double) totalRows / infoPage.getPageSize());
    //      infoPage.setTotalPages((int) div);
    //      if (infoPage.getPageSize() > totalRows) {
    //        result = false;
    //        div = 0.0;
    //        infoPage.setTotalPages((int) div);
    //      }
    //    }
    //    ConsultaResponse consultaResponse = consultaResponseFactory.newConsultaResponse(
    //      res,
    //      infoPage
    //    );
    //
    //    result = result && infoPage.getPageNumber() <= infoPage.getTotalPages();
    //
    //    return result
    //      ? ResponseEntity.ok().body(consultaResponse)
    //      : ResponseEntity.badRequest().build();
  }

    @PostMapping("/addConsulta")
    public ResponseEntity<Void> nuevaConsulta(@RequestBody Consulta consultaRequest) throws Exception {
        consultaDAO.insertarConsulta(consultaRequest);
        return ResponseEntity.ok().build();
    }

   /* @PostMapping("/eliminarConsulta")
    public ResponseEntity<Void> eliminarConsulta(@RequestBody Integer idConsulta) throws SQLException{
      consultaDAO.eliminarYActualizarById(idConsulta);
      return ResponseEntity.ok().build();
    } */
}
