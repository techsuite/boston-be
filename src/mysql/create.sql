USE boston;

CREATE TABLE Usuario(
    dni VARCHAR(9) NOT NULL,
    nombre VARCHAR(50) NOT NULL,
    apellidos VARCHAR(100) NOT NULL,
    email VARCHAR(100) NOT NULL,
    contrasena VARCHAR(20) NOT NULL,
    fechaDeNacimiento DATE NOT NULL,
    genero VARCHAR(10) NOT NULL,
    PRIMARY KEY(dni)
);

INSERT INTO Usuario
VALUES 
("00000000v", "Nombre0", "Apellido0","usuario0@mail.com", "asdf", CAST('1983-06-10' AS DATE) , "Masculino"),
("11111111k", "nombre1", "Apellido1","usuario1@mail.com", "1234", CAST('1975-10-04' AS DATE) , "Femenino"),
("12345678l", "NombreL", "ApellidoL","L@mail.com","AB12", CAST('1999-09-11' AS DATE) , "Masculino");

CREATE TABLE Paciente(
    DNI VARCHAR(9) NOT NULL,
    nombre VARCHAR(45) NOT NULL,
    apellidos VARCHAR(45) NOT NULL,
    email VARCHAR(45) NOT NULL,
    fecha_registro DATE NOT NULL,
    fecha_ult_consulta DATE NULL,
    fecha_nacimiento DATE NOT NULL,
    genero VARCHAR(45) NOT NULL,
    PRIMARY KEY(DNI)
);

CREATE TABLE Consulta(
    id_consulta INT NOT NULL AUTO_INCREMENT,
    DNI_usuario VARCHAR(9) NOT NULL,
    DNI_paciente VARCHAR(9) NOT NULL,
    fecha_consulta DATE NULL,
    enfermedades_diag VARCHAR(45) NULL,
    medicamentos_recetados VARCHAR(45) NULL,
    pruebas_lab VARCHAR(45) NULL,
    observacion VARCHAR(45) NULL,
    PRIMARY KEY(id_consulta),
    INDEX DNI_usuario (DNI_usuario ASC) VISIBLE,
    INDEX DNI_paciente (DNI_paciente ASC) VISIBLE,
    CONSTRAINT dni_usuario_fk
    FOREIGN KEY (dni_usuario)
    REFERENCES boston.Usuario (dni)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
    FOREIGN KEY (dni_paciente)
    REFERENCES boston.Paciente (DNI)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE Relacion(
 id int NOT NULL AUTO_INCREMENT,
 DNI_usuario varchar(9) NOT NULL,
 DNI_paciente varchar(9) NOT NULL,
 CONSTRAINT DNI_not_equal check (DNI_usuario <> DNI_paciente),
 UNIQUE (DNI_usuario, DNI_paciente),
 PRIMARY KEY(id),
 FOREIGN KEY (DNI_usuario)
 REFERENCES boston.Usuario(dni)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION,
 FOREIGN KEY (DNI_paciente)
 REFERENCES boston.Paciente(DNI)
 ON DELETE NO ACTION
 ON UPDATE NO ACTION
 );
)
