package app.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import app.model.Paciente;
import app.model.response.InfoPage;

import java.sql.SQLException;
import java.sql.Date;
import java.util.List;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PacienteDaoTest extends GenericDaoTest {

  @Autowired
  private PacienteDao pacienteDAO;

  @Test
  public void getPacienteByDNI_whenCall_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true);
    pacienteDAO.getPacienteByDNI("01234567v");

    verify(preparedStatement).setString(1, "01234567v");
  }

  @Test(expected = SQLException.class)
  public void getEmpleado_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    pacienteDAO.getPacienteByDNI("");
  }

  @Test
  public void getPacienteByDNI_whenCallAndNoResult() throws Exception {
    when(resultSet.next()).thenReturn(false);
    Paciente output = pacienteDAO.getPacienteByDNI("--");

    assertThat(output).isNull();
  }

  @Test
  public void getPacienteByDNI_whenCallAndResult() throws Exception {
    when(resultSet.next()).thenReturn(true);

    Paciente output = pacienteDAO.getPacienteByDNI("0123456x");

    assertThat(output).isNotNull();
  }

  @Test(expected = SQLException.class)
  public void getPacientes_whenCallAndFails() throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    List<Paciente> output = pacienteDAO.getPacientesPagesFromQuery(new InfoPage(1,1,1));
  }

  @Test
  public void getPacientes_whenCallAndNoPacientes() throws Exception {
    when(resultSet.next()).thenReturn(false);

    List<Paciente> output = pacienteDAO.getPacientesPagesFromQuery(new InfoPage(1,1,1));

    assertThat(output.size() == 0).isTrue();
  }

  @Test(expected = SQLException.class)
  public void insertarPaciente_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt() throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    Paciente p = new Paciente();
    p.setDni("a");
    
    pacienteDAO.insertarPaciente(p);
  }

  @Test(expected = SQLException.class)
  public void insertarPaciente_whenTablaNoPermiteInsertar() throws Exception {
    when(preparedStatement.executeUpdate())
      .thenThrow(SQLException.class);
    Paciente p = new Paciente("76543210j", "A", "B", "AB@email.com", new Date(0), new Date(1), new Date(2), "X");
    pacienteDAO.insertarPaciente(p);
  }

  @Test(expected = NullPointerException.class)
  public void insertarPaciente_whenNoTieneDNI_throwException() throws Exception {
    Paciente p = new Paciente();

    pacienteDAO.insertarPaciente(p);
  }

  @Test
  public void insertarPaciente_whenCallAndResult() throws Exception {
    Paciente p = new Paciente();
    p.setDni("a");

    pacienteDAO.insertarPaciente(p);
  }

  @Test(expected = NullPointerException.class)
  public void actualizarFecha_whenNoDNI_throwException() throws Exception {
    pacienteDAO.actualizarFecha(null, new Date(1));
  }

  @Test(expected = SQLException.class)
  public void actualizarFecha_whenNoDNIInTable_throwException() throws Exception {
    when(preparedStatement.executeUpdate())
      .thenThrow(SQLException.class);
    pacienteDAO.actualizarFecha("-", new Date(1));
  }

  @Test
  public void actualizarFecha_whenSuccess() throws Exception {
    doNothing().when(preparedStatement).setString(anyInt(), anyString());
    doNothing().when(preparedStatement).setDate(anyInt(), any());

    when(preparedStatement.executeUpdate()).thenReturn(1);
    doNothing().when(preparedStatement).close();

    boolean result = pacienteDAO.actualizarFecha("-", new Date(1));
    assertThat(result).isTrue();
  }

  @Test
  public void actualizarFecha_whenNoSuccess() throws Exception {
    doNothing().when(preparedStatement).setString(anyInt(), anyString());
    doNothing().when(preparedStatement).setDate(anyInt(), any());

    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();

    boolean result = pacienteDAO.actualizarFecha("-", new Date(1));
    assertThat(result).isFalse();
  }

  @Test
  public void getPacientes_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    when(resultSet.getString(1)).thenReturn("a");
    when(resultSet.getString(2)).thenReturn("a");
    when(resultSet.getString(3)).thenReturn("a");
    when(resultSet.getString(4)).thenReturn("a");
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(7)).thenReturn("a");
   
    Paciente paciente = new Paciente("a","a","a","a",Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),"a");


    List<Paciente> output = pacienteDAO.getPacientesPagesFromQuery(new InfoPage(1,1,1));

    assertThat(output.get(0).getDni()).isEqualTo(paciente.getDni());
  }


}
