package app.dao;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import app.model.Consulta;
import app.model.Usuario;
import app.model.Paciente;
import app.model.response.InfoPage;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.sql.SQLException;
import java.sql.Date;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.EscapedErrors;
import org.springframework.boot.test.mock.mockito.MockBean;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ConsultaDaoTest extends GenericDaoTest {

  
  @Mock
  protected ConsultaDao consultaDao;

  @Autowired
  private ConsultaDao consultaDAO;

  InfoPage ip = new InfoPage(2, 2, 2);

  @Test(expected = SQLException.class)
  public void getConsultaByDNI_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    consultaDAO.getConsultaByDNI("-", ip);
  }

  @Test
  public void getConsultaByDNI_whenCallAndNoResult() throws Exception {
    when(resultSet.next()).thenReturn(false);
    List<Consulta> output = consultaDAO.getConsultaByDNI("-", ip);

    assertThat(output.size() == 0).isTrue();
  }


  @Test(expected = SQLException.class)
  public void getConsultaPage_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    consultaDAO.getConsultaPagesFromQuery(ip);
  }

  @Test
  public void getConsultaPage_whenCallAndNoResult() throws Exception {
    when(resultSet.next()).thenReturn(false);
    List<Consulta> output = consultaDAO.getConsultaPagesFromQuery(ip);

    assertThat(output.size() == 0).isTrue();
  }

  @Test(expected = NullPointerException.class)
  public void insertarConsulta_whenCalledWithNull() throws Exception {
    Consulta c = null;
    consultaDAO.insertarConsulta(c);
  }

  @Test
  public void insertarConsulta_whenCalledIsntInserted() throws Exception {
    Consulta c = new Consulta();
    c.setDniPaciente("a");
    c.setDniDoctor("b");

    when(connection.prepareStatement(anyString(), anyInt()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), any());
    doNothing().when(preparedStatement).setDate(anyInt(), any());
    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();
    when(preparedStatement.getGeneratedKeys()).thenReturn(resultSet);
    when(resultSet.next()).thenReturn(false);
    Integer pk = consultaDAO.insertarConsulta(c);
    assertThat(pk == -1).isTrue();
  }

  @Test
  public void insertarConsulta_whenCalledIsInserted() throws Exception {
    Consulta c = new Consulta();
    c.setDniPaciente("a");
    c.setDniDoctor("b");

    when(connection.prepareStatement(anyString(), anyInt()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), any());
    doNothing().when(preparedStatement).setDate(anyInt(), any());
    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();
    when(preparedStatement.getGeneratedKeys()).thenReturn(resultSet);
    when(resultSet.next()).thenReturn(true);
    when(resultSet.getInt(anyInt())).thenReturn(20);
    Integer pk = consultaDAO.insertarConsulta(c);
    assertThat(pk == 20).isTrue();
  }

  @Test(expected = SQLException.class)
  public void insertarConsulta_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString(), anyInt()))
      .thenThrow(SQLException.class);
    Consulta c = new Consulta();
    consultaDAO.insertarConsulta(c);
  }
  @Test
  public void getConsulta_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    when(resultSet.getInt(1)).thenReturn(1);
    when(resultSet.getDate(20)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(4)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(5)).thenReturn("a");
    when(resultSet.getString(6)).thenReturn("a");
    when(resultSet.getString(7)).thenReturn("a");
    when(resultSet.getString(8)).thenReturn("a");
    when(resultSet.getString(9)).thenReturn("a");
    when(resultSet.getString(10)).thenReturn("a");
    when(resultSet.getString(11)).thenReturn("a");
    when(resultSet.getDate(14)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(12)).thenReturn("a");
    when(resultSet.getString(15)).thenReturn("a");
    when(resultSet.getString(13)).thenReturn("a");
    when(resultSet.getString(16)).thenReturn("a");
    when(resultSet.getString(17)).thenReturn("a");
    when(resultSet.getString(18)).thenReturn("a");
    when(resultSet.getDate(22)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(19)).thenReturn("a");
    when(resultSet.getString(23)).thenReturn("a");
    
   
    Consulta c = new Consulta(1,Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),"a","a","a","a","a","a","a",
    Date.valueOf("1111-11-11"),"a","a","a","a","a","a",Date.valueOf("1111-11-11"),"a","a");


    List<Consulta> output = consultaDAO.getConsultaByDNI("a", ip);

    assertThat(output.get(0).getIdConsulta()).isEqualTo(c.getIdConsulta());
  }

  @Test
  public void getConsultaSinDni_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    when(resultSet.getInt(1)).thenReturn(1);
    when(resultSet.getDate(20)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(4)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(5)).thenReturn("a");
    when(resultSet.getString(6)).thenReturn("a");
    when(resultSet.getString(7)).thenReturn("a");
    when(resultSet.getString(8)).thenReturn("a");
    when(resultSet.getString(9)).thenReturn("a");
    when(resultSet.getString(10)).thenReturn("a");
    when(resultSet.getString(11)).thenReturn("a");
    when(resultSet.getDate(14)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(12)).thenReturn("a");
    when(resultSet.getString(15)).thenReturn("a");
    when(resultSet.getString(13)).thenReturn("a");
    when(resultSet.getString(16)).thenReturn("a");
    when(resultSet.getString(17)).thenReturn("a");
    when(resultSet.getString(18)).thenReturn("a");
    when(resultSet.getDate(22)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(19)).thenReturn("a");
    when(resultSet.getString(23)).thenReturn("a");
    
   
    Consulta c = new Consulta(1,Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),"a","a","a","a","a","a","a",
    Date.valueOf("1111-11-11"),"a","a","a","a","a","a",Date.valueOf("1111-11-11"),"a","a");


    List<Consulta> output = consultaDAO.getConsultaPagesFromQuery(ip);

    assertThat(output.get(0).getIdConsulta()).isEqualTo(c.getIdConsulta());
  }

/*
  @Test(expected = NullPointerException.class)
  public void actualizarConsulta_whenNullThrowsNull() throws Exception {
    Consulta c = null;
    consultaDAO.actualizarConsulta(c);
  }

  @Test(expected = SQLException.class)
  public void actualizarConsulta_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt() throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    Consulta c = new Consulta();
    consultaDAO.actualizarConsulta(c);
  }

  
  @Test
  public void actualizarConsulta_whenIsUpdated() throws Exception {
    Consulta c = new Consulta();
    c.setDniPaciente("a");
    c.setDniDoctor("b");

    when(connection.prepareStatement(anyString()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), anyString());
    doNothing().when(preparedStatement).setInt(anyInt(), anyInt());
    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();

    consultaDAO.actualizarConsulta(c);
  }

  @Test(expected = SQLException.class)
  public void actualizarConsulta_whenIsntInsertedDueToError() throws Exception {
    Consulta c = new Consulta();
    c.setDniPaciente("a");
    c.setDniDoctor("b");

    when(connection.prepareStatement(anyString()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), anyString());
    doNothing().when(preparedStatement).setInt(anyInt(), anyInt());
    when(preparedStatement.executeUpdate())
      .thenThrow(SQLException.class);

    consultaDAO.actualizarConsulta(c);
  }

  @Test(expected = SQLException.class)
  public void actualizarConsulta_whenIsntUpdated() throws Exception {
    Consulta c = new Consulta();
    c.setDniPaciente("a");
    c.setDniDoctor("b");

    when(connection.prepareStatement(anyString()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), anyString());
    doNothing().when(preparedStatement).setInt(anyInt(), anyInt());
    when(preparedStatement.executeUpdate())
      .thenThrow(SQLException.class);

    consultaDAO.actualizarConsulta(c);
  }
*/
  @Test(expected = SQLException.class)
  public void eliminarConsulta_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt() throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    consultaDAO.eliminarConsultaByID(12);
  }

 @Test
  public void eliminarConsulta_whenCalledAndSuccess() throws Exception {
    when(preparedStatement.executeUpdate())
      .thenReturn(1);
    
    assertThat(consultaDAO.eliminarConsultaByID(1)).isTrue();
  }

  @Test
  public void eliminarConsulta_whenCalledAndNoDelete() throws Exception {
    doNothing().when(preparedStatement).setInt(anyInt(), anyInt());
    when(preparedStatement.executeUpdate())
      .thenReturn(0);
    assertThat(consultaDAO.eliminarConsultaByID(12)).isFalse();
  }

  @Test
  public void getIdPacienteByIdConsulta_setCorrectParams() throws Exception{
    
    when(resultSet.next()).thenReturn(true);
    consultaDAO.getIdPacienteByIdConsulta(55);

    verify(preparedStatement).setInt(1, 55);

  }
/*
  @Test
  public void getNuevaFechaConsulta_setCorrectParams() throws Exception{    
    ConsultaDao c= Mockito.spy(consultaDAO);
    Mockito.doReturn(1).when(c).getNumConsultasPaciente("a");

    when(resultSet.next()).thenReturn(true);
    when(resultSet.getDate(1)).thenReturn(Date.valueOf("1111-11-11"));

    Date output=consultaDAO.getNuevaFechaConsulta("a");

    assertThat(output).isEqualTo(Date.valueOf("1111-11-11"));

  }

  @Test
  public void eliminarYActualizarById_whenCalledAndFail() throws Exception{
    when(consultaDao.getIdPacienteByIdConsulta(0)).thenReturn(null);
    assertThat(consultaDAO.eliminarYActualizarById(0)).isFalse();
  } */
}
