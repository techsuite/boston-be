package app.dao;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

import app.model.Usuario;
import app.model.response.InfoPage;
import app.model.Paciente;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RelacionDaoTest extends GenericDaoTest {

  @Autowired
  private RelacionDao relacionDAO;

  InfoPage ip = new InfoPage(2, 1, 0);

  @Test(expected = SQLException.class)
  public void obtenerPacienteAsociadoDNIUsuario_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    relacionDAO.obtenerPacienteAsociadoDNIUsuario("-","-",ip);
  }

  @Test
  public void obtenerPacienteAsociadoDNIUsuario_whenCallAndNoResult() throws Exception {
    when(resultSet.next()).thenReturn(false);
    List<Paciente> output = relacionDAO.obtenerPacienteAsociadoDNIUsuario("-","-",ip);

    assertThat(output.size() == 0).isTrue();
  }

  @Test(expected = SQLException.class)
  public void obtenerPacienteNoAsociadoDNIUsuario_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);
    relacionDAO.obtenerPacienteNoAsociadoDNIUsuario("-","-",ip);
  }

  @Test
  public void obtenerPacienteNoAsociadoDNIUsuario_whenCallAndNoResult() throws Exception {
    when(resultSet.next()).thenReturn(false);
    List<Paciente> output = relacionDAO.obtenerPacienteNoAsociadoDNIUsuario("-","-",ip);

    assertThat(output.size() == 0).isTrue();
  }

  @Test(expected = NullPointerException.class)
  public void insertarRelacion_whenCalledWithNull() throws Exception {
    String usuario = null;
    String paciente = null;
    relacionDAO.insertarRelacion(usuario, paciente);
  }

  @Test
  public void insertarRelacion_whenCalledIsntInserted() throws Exception {
    String usuario = "a";
    String paciente = "b";

    when(connection.prepareStatement(anyString(), anyInt()))
      .thenReturn(preparedStatement);
    doNothing().when(preparedStatement).setString(anyInt(), any());
    when(preparedStatement.executeUpdate()).thenReturn(0);
    doNothing().when(preparedStatement).close();
    relacionDAO.insertarRelacion(usuario,paciente);
  
  }

  @Test(expected = SQLException.class)
  public void insertarConsulta_whenCalledIsInserted() throws Exception {
    String usuario = "00000000v";
    String paciente = "11111111k";

    when(preparedStatement.executeUpdate())
      .thenThrow(SQLException.class);
    relacionDAO.insertarRelacion(usuario, paciente);
  }

  /*public void comprobarSiRelacionExiste_whenCall_executeRunnerQuery() throws Exception {
    Usuario usuario = new Usuario();
    Paciente paciente = new Paciente();
    usuario.setDni("00000000v");
    paciente.setDni("11111111k");
    relacionDAO.comprobarSiRelacionExiste(usuario, paciente);

    verify(queryRunner)
      .query(eq(connection), anyString(), any(BeanHandler.class), eq("00000000v"),eq("11111111k"));
  }

  @Test
  public void comprobarSiRelacionExiste_setCorrectParams() throws Exception {
    when(resultSet.isBeforeFirst()).thenReturn(true);
    
    Usuario usuario = new Usuario();
    Paciente paciente = new Paciente();
    usuario.setDni("00000000v");
    paciente.setDni("11111111k");

    Boolean output = relacionDAO.comprobarSiRelacionExiste(usuario, paciente);

    assertThat(output).isTrue();
  }

  public void comprobarSiRelacionExiste_setIncorrectParams() throws Exception {
    when(resultSet.isBeforeFirst()).thenReturn(false);
    
    Usuario usuario = new Usuario();
    Paciente paciente = new Paciente();
    usuario.setDni("00000000v");
    paciente.setDni("11111111k");

    Boolean output = relacionDAO.comprobarSiRelacionExiste(usuario, paciente);

    assertThat(output).isFalse();
  }*/

 

@Test
  public void getUsuariosAsociados_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    when(resultSet.getString(1)).thenReturn("a");
    when(resultSet.getString(2)).thenReturn("a");
    when(resultSet.getString(3)).thenReturn("a");
    when(resultSet.getString(4)).thenReturn("a");
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(7)).thenReturn("a");
   
    Paciente paciente = new Paciente("a","a","a","a",Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),"a");


    List<Paciente> output = relacionDAO.obtenerPacienteAsociadoDNIUsuario("b"," ", ip);

    assertThat(output.get(0).getDni()).isEqualTo(paciente.getDni());
  }
  
  @Test
  public void getUsuariosNoAsociados_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true).thenReturn(false);
    when(resultSet.getString(1)).thenReturn("a");
    when(resultSet.getString(2)).thenReturn("a");
    when(resultSet.getString(3)).thenReturn("a");
    when(resultSet.getString(4)).thenReturn("a");
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getDate(5)).thenReturn(Date.valueOf("1111-11-11"));
    when(resultSet.getString(7)).thenReturn("a");
   
    Paciente paciente = new Paciente("a","a","a","a",Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),Date.valueOf("1111-11-11"),"a");


    List<Paciente> output = relacionDAO.obtenerPacienteNoAsociadoDNIUsuario("b"," ", ip);

    assertThat(output.get(0).getDni()).isEqualTo(paciente.getDni());
  }

}