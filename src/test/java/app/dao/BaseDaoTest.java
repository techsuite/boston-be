package app.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import app.model.Usuario;
import app.model.response.InfoPage;

import java.util.List;
import java.util.ArrayList;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Connection;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseDaoTest extends GenericDaoTest {

  //Usamos UsuarioDao para acceder a las funciones de BaseDao
  // ya que acceder directamente a BaseDao da problemas
  @Autowired
  private UsuarioDao baseDAO;

  @Test
  public void numOfRows_whenCalledAndSuccess()
    throws Exception {
    when(queryRunner.query(any(Connection.class), anyString(), any()))
      .thenReturn(new Long(1));
    long res = baseDAO.numOfRows();
    assertThat(res == 1).isTrue();
  }

  @Test
  public void numOfPages_whenCalledAndSuccess()
    throws Exception {
    long res = baseDAO.numOfPages(new Long(2), new Long(5));
    assertThat(res == 3).isTrue();
    res = baseDAO.numOfPages(new Long(2), new Long(6));
    assertThat(res == 3).isTrue();
  }

  @Test(expected = NullPointerException.class)
  public void getPagesFromQuery_whenCalledAndNoArgument()
    throws Exception {
    baseDAO.getPagesFromQuery("", null, Usuario.class);
  }


  @Test
  public void getPagesFromQuery_whenCalledAndSuccess()
    throws Exception {
    List<Usuario> ls = new ArrayList<Usuario>();
    ls.add(null);
    ls.add(null);
    when(queryRunner.query(any(Connection.class), anyString(), any()))
      .thenReturn(ls);
    baseDAO.getPagesFromQuery("", new InfoPage(1,1,1), Usuario.class);
  
    assertThat(ls.size() == 2).isTrue();
  }

}
