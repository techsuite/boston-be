package app.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

import app.model.Usuario;

import java.sql.Date;
import java.sql.SQLException;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioDaoTest extends GenericDaoTest {

  @Autowired
  private UsuarioDao usuarioDAO;

  @Test
  public void getUsuarioByDNI_whenCall_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true);
    usuarioDAO.getUsuarioByDNI("nick1");

    verify(preparedStatement).setString(1, "nick1");
  }

  @Test(expected = SQLException.class)
  public void getEmpleado_whenCalledAndAnExceptionIsThrownInPrepareStatement_shouldThrowIt()
    throws Exception {
    when(connection.prepareStatement(anyString()))
      .thenThrow(SQLException.class);

    usuarioDAO.getUsuarioByDNI("x");
  }

  @Test
  public void getUsuarioByDNI_whenCallAndNoResult() throws Exception {
    usuarioDAO = new UsuarioDao();

    Usuario output = usuarioDAO.getUsuarioByDNI("jaime");

    assertThat(output).isNull();
  }

  @Test
  public void getUsuarioByMayuscDNI_whenCallAndNoResult() throws Exception {
    //usuarioDAO = new UsuarioDao();

    Usuario output = usuarioDAO.getUsuarioByDNI("NICK1");

    assertThat(output).isNull();
  }

  @Test
  public void getUsuarioByDNI_whenCallAndResult() throws Exception {
    when(resultSet.next()).thenReturn(true);

    Usuario output = usuarioDAO.getUsuarioByDNI("nick1");

    assertThat(output).isNotNull();
  }

  @Test
  public void getUsuarioByDNI2_whenCall_executeRunnerQuery() throws Exception {
    usuarioDAO.getUsuarioByDNI2("nick");

    verify(queryRunner)
      .query(eq(connection), anyString(), any(BeanHandler.class), eq("nick"));
  }

  @Test
  public void verificacionCredenciales_setCorrectParams() throws Exception {
    when(resultSet.next()).thenReturn(true);
    when(resultSet.getString(1)).thenReturn("00000000v");
    when(resultSet.getString(2)).thenReturn("Nombre0");
    when(resultSet.getString(3)).thenReturn("Apellido0");
    when(resultSet.getString(4)).thenReturn("usuario0@mail.com");
    when(resultSet.getString(5)).thenReturn("asdf");
    when(resultSet.getDate(6)).thenReturn(Date.valueOf("1983-06-10"));
    when(resultSet.getString(7)).thenReturn("Masculino");

    Usuario usuario = new Usuario();
    usuario.setDni("00000000v");
    usuario.setContrasena("asdf");

    Boolean output = usuarioDAO.verificacionCredendiales(usuario);

    assertThat(output).isTrue();
  }
}
