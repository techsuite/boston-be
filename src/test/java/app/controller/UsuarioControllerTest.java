package app.controller;

import app.dao.UsuarioDao;
import app.model.Usuario;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.Date;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private UsuarioDao usuarioDAO;

  @Test
  public void getUsuario_whenUserCallNoExist() throws Exception{
    when(usuarioDAO.getUsuarioByDNI2(anyString())).thenReturn(null);
    when(usuarioDAO.getUsuarioByDNI(anyString())).thenReturn(null);

    mockMvc.perform(get("/user/notExisting")).andExpect(status().isNoContent());
  }


  @Test
  public void getUsuario_andReturnThem()
    throws Exception {
      Usuario user = new Usuario("0000000v","AAA", "GM", "continue@gotvg.com", "0412", Date.valueOf("1900-01-01") ,"Masculino");
      when(usuarioDAO.getUsuarioByDNI("0000000v")).thenReturn(user);
      when(usuarioDAO.getUsuarioByDNI2("0000000v")).thenReturn(user);

    mockMvc
    .perform(get("/user/0000000v"))
    .andExpect(status().isOk())
    .andExpect(jsonPath("$[*]", hasSize(7)))
    .andExpect(jsonPath("$.dni", is(user.getDni())))
    .andExpect(jsonPath("$.nombre", is(user.getNombre())))
    .andExpect(jsonPath("$.apellidos", is(user.getApellidos())))
    .andExpect(jsonPath("$.email", is(user.getEmail())))
    .andExpect(jsonPath("$.contrasena", is(user.getContrasena())))
    .andExpect(jsonPath("$.fechaDeNacimiento", is(user.getFechaDeNacimiento().toString())))
    .andExpect(jsonPath("$.genero", is(user.getGenero())));
  }

  @Test
    public void verificarCredenciales_whenUserIsCorrect() throws Exception{

        when(usuarioDAO.verificacionCredendiales(any())).thenReturn(true);

        mockMvc.perform(get("/check/nick1/asdf123"))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$[*]", hasSize(0)))
            .andExpect(jsonPath("$", is(true)));
    }
  
}
