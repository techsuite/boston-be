package app.controller;

import app.dao.PacienteDao;
import app.dao.RelacionDao;
import app.model.Paciente;
import app.model.Relacion;
import app.model.Usuario;
import app.model.request.PacienteFechaRequest;
import app.model.request.RelacionRequest;
import app.model.response.InfoPage;

import java.util.List;
import java.util.ArrayList;

import java.sql.SQLException;
import java.sql.Date;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(PacienteController.class)
public class PacienteControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private PacienteDao pacienteDAO;

  @MockBean
  private RelacionDao relacionDAO;

  @MockBean
  private Paciente pacienteMock;

  @MockBean
  private InfoPage info;


  @Test
  public void getPaciente_wrongURL() throws Exception{
    mockMvc
      .perform(get("/getPacientes"))
      .andExpect(status().isNotFound());
  }

  @Test
  public void getPaciente_andDoesntWork() throws Exception{
    mockMvc
      .perform(get("/getPacientes/dni/ /pagina"))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void getPaciente2_andDoesntWork() throws Exception{
    mockMvc
      .perform(get("/getPacientes/aaaaaaaaaa/ /pagina"))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void pacientes_andReturnsNull() throws Exception{
    when(pacienteDAO.getPacientesPagesFromQuery(null))
      .thenReturn(null);

    mockMvc
    .perform(get("/pacientes?pageNumber=&pageSize="))
    .andExpect(status().isBadRequest());
  }

 
  @Test
  public void getPaciente_andReturnThem() throws Exception{
    Paciente paciente = new Paciente("54867923j", "Paula", "Garcia", "paula_garcia@gmail,com", 
    Date.valueOf("2021-02-18"), null, Date.valueOf("1972-02-02"),"femenino");

    List<Paciente> ls = new ArrayList<>();
    ls.add(paciente);

    when(relacionDAO.getMetadataFromQuery(any(), anyString()))
      .thenReturn(new InfoPage());

    when(relacionDAO.obtenerPacienteAsociadoDNIUsuario(any(), any(), any()))
      .thenReturn(ls);

    mockMvc
      .perform(get("/getPacientes/dni/dniPac/pagina"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(2)))
      .andExpect(jsonPath("$.pacientes", hasSize(1)))
      .andExpect(jsonPath("$.pacientes[0].dni", is(paciente.getDni())))
      .andExpect(jsonPath("$.pacientes[0].nombre", is(paciente.getNombre())))
      .andExpect(jsonPath("$.pacientes[0].apellido", is(paciente.getApellido())))
      .andExpect(jsonPath("$.pacientes[0].email", is(paciente.getEmail())))
      .andExpect(jsonPath("$.pacientes[0].fechaRegistro", is(paciente.getFechaRegistro().toString())))
      .andExpect(jsonPath("$.pacientes[0].fechaUltimaConsulta", is(paciente.getFechaUltimaConsulta())))
      .andExpect(jsonPath("$.pacientes[0].fechaNacimiento", is(paciente.getFechaNacimiento().toString())))
      .andExpect(jsonPath("$.pacientes[0].genero", is(paciente.getGenero())));
  }

  @Test
  public void getNoPaciente_wrongURL() throws Exception{
    mockMvc
      .perform(get("/getNoPacientes"))
      .andExpect(status().isNotFound());
  }

  @Test
  public void getNoPaciente_andDoesntWork() throws Exception{
    mockMvc
      .perform(get("/getNoPacientes/dni/ /pagina"))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void getNoPaciente2_andDoesntWork() throws Exception{
    mockMvc
      .perform(get("/getPacientes/aaaaaaaaaa/ /pagina"))
      .andExpect(status().isBadRequest());
  }


  @Test
  public void getNoPaciente_andReturnThem() throws Exception{
    Paciente paciente2 = new Paciente("54867923j", "Paula", "Garcia", "paula_garcia@gmail,com", 
    Date.valueOf("2021-02-18"), null, Date.valueOf("1972-02-02"),"femenino");

    Paciente paciente1 = new Paciente("12345678b", "Nombre", "apellidos", "correodoctor@gotvg.com", 
    Date.valueOf("2020-03-04"), Date.valueOf("2021-03-02"), Date.valueOf("1989-10-11"),"Masculino");

    Paciente paciente3 = new Paciente("12345678c", "Nombre", "apellidos", "correodoctor@gotvg.com", 
    Date.valueOf("2020-03-04"), Date.valueOf("2021-03-02"), Date.valueOf("1989-10-11"),"Masculino");

    Paciente paciente4 = new Paciente("12345678d", "Nombre", "apellidos", "correodoctor@gotvg.com", 
    Date.valueOf("2020-03-04"), Date.valueOf("2021-03-02"), Date.valueOf("1989-10-11"),"Masculino");

    Usuario user = new Usuario("0000000v","AAA", "GM", "continue@gotvg.com", "0412", Date.valueOf("1900-01-01") ,"Masculino");
    Relacion re1 = new Relacion(1, user.getDni(),paciente1.getDni());
    Relacion re2 = new Relacion(2, user.getDni(),paciente3.getDni());
    Relacion re3 = new Relacion(3, user.getDni(), paciente4.getDni());
    List<Relacion> la = new ArrayList<>();
    la.add(re1);
    la.add(re2);
    la.add(re3);

    List<Paciente> ls = new ArrayList<>();
    ls.add(paciente4);

    when(relacionDAO.getMetadataFromQuery(any(), anyString()))
      .thenReturn(new InfoPage());

    when(relacionDAO.obtenerPacienteNoAsociadoDNIUsuario(any(), any(), any()))
      .thenReturn(ls);

    mockMvc
      .perform(get("/getNoPacientes/dni/dniPac/pagina"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(2)))
      .andExpect(jsonPath("$.pacientes", hasSize(1)))
      .andExpect(jsonPath("$.pacientes[0].dni", is(paciente4.getDni())))
      .andExpect(jsonPath("$.pacientes[0].nombre", is(paciente4.getNombre())))
      .andExpect(jsonPath("$.pacientes[0].apellido", is(paciente4.getApellido())))
      .andExpect(jsonPath("$.pacientes[0].email", is(paciente4.getEmail())))
      .andExpect(jsonPath("$.pacientes[0].fechaRegistro", is(paciente4.getFechaRegistro().toString())))
      .andExpect(jsonPath("$.pacientes[0].fechaUltimaConsulta", is(paciente4.getFechaUltimaConsulta().toString())))
      .andExpect(jsonPath("$.pacientes[0].fechaNacimiento", is(paciente4.getFechaNacimiento().toString())))
      .andExpect(jsonPath("$.pacientes[0].genero", is(paciente4.getGenero())));
  }
  @Test
  public void addPaciente_whenWorks() throws Exception {
    Paciente paciente = new Paciente("12345678v", "Nombre", "apellidos", "correodoctor@gotvg.com", 
    Date.valueOf("2020-03-04"), Date.valueOf("2021-03-02"), Date.valueOf("1989-10-11"),"Masculino");

    mockMvc.perform(post("/insertarNuevo")
      .content(new JSONObject(paciente).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(content().string(""));
  }

  @Test
  public void asociarPaciente_whenWorks() throws Exception {
    RelacionRequest relacionRequest = new RelacionRequest("76543210j", "12345678l");
    mockMvc.perform(post("/insertarNuevo")
      .content(new JSONObject(relacionRequest).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(content().string(""));
  }


  @Test
  public void actualizarFechaConsulta_whenWorks() throws Exception{
      String dniPaciente = "76543210j";
      Date fecha = Date.valueOf("2021-03-28");
      PacienteFechaRequest pacienteFechaRequest = new PacienteFechaRequest(dniPaciente,fecha);
      mockMvc.perform(post("/actualizarFechaConsulta")
      .content(new JSONObject(pacienteFechaRequest).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(content().string(""));
  }

  @Test
  public void insertarNuevo_whenWorks() throws Exception{
    String u= "76543210j";
    String p= "76543210a";
      RelacionRequest r = new RelacionRequest(u,p);
      mockMvc.perform(post("/asociarPaciente")
      .content(new JSONObject(r).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(content().string(""));
  }
}

