package app.controller;

import app.dao.ConsultaDao;
import app.dao.PacienteDao;
import app.model.Consulta;
import app.model.response.InfoPage;

import java.util.List;
import java.util.ArrayList;

import java.sql.SQLException;
import java.sql.Date;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.http.MediaType;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(ConsultaController.class)
public class ConsultaControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ConsultaDao consultaDAO;

  @MockBean
  private Consulta consultaMock;

  @MockBean
  private InfoPage info;

  @MockBean
  private PacienteDao pacienteDAO;


  @Test
  public void getConsulta_wrongURL() throws Exception{
    mockMvc
      .perform(get("/consultasDNI"))
      .andExpect(status().isNotFound());
  }

  @Test
  public void getConsulta_andDoesntWork2() throws Exception{
    mockMvc
      .perform(get("/consultasDNI/aaaaaaaaaa/pagina"))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void getConsulta_andDoesntWork() throws Exception{
    mockMvc
      .perform(get("/consultasDNI/dni/pagina"))
      .andExpect(status().isBadRequest());
  }

  //Esta excepcion esta causada porque asdf no es un numero
  @Test
  public void consultas_badURLNoNums() throws Exception{
    mockMvc
      .perform(get("/consultas?pageNumber=132&pageSize=asdf"))
      .andExpect(status().isBadRequest());
  }
  @Test
  public void consultas_when_called_firstIF_WrongFFF() throws Exception {
      mockMvc
              .perform(get("/consultas?pageNumber=0&pageSize=0"))
              .andExpect(status().isBadRequest());

  }

  @Test
  public void consultas_when_called_firstIF_WrongTFF() throws Exception {
      mockMvc
              .perform(get("/consultas?pageNumber=1&pageSize=0"))
              .andExpect(status().isBadRequest());
  }

  @Test
  public void consultas_when_called_null() throws Exception {
      mockMvc
              .perform(get("/consultas?pageNumber=&pageSize="))
              .andExpect(status().isBadRequest());
  }

  @Test
  public void consultas_when_called_firstIF_WrongTTF() throws Exception {
      mockMvc
              .perform(get("/consultas?pageNumber=0&pageSize=1"))
              .andExpect(status().isBadRequest());
  }



  @Test
  public void consultas_whenWorks() throws Exception{
    Consulta consulta = new Consulta(1234, Date.valueOf("2021-03-03"), Date.valueOf("2021-04-01"), 
    "Enfermedad", "Medicamento", "Pruebas Laboratorio", "Observaciones", "11111111V", "NombreDoctor", 
    "ApellidosDoctor", Date.valueOf("1980-01-01") , "correodoctor@gotvg.com", "Masculino", "0412", 
    "00000000F", "NombrePaciente",  "ApellidoPaciente", Date.valueOf("1990-01-01") , 
    "correopaciente@gotvg.com", "Masculino");

    when(consultaDAO.getMetadata(any()))
      .thenReturn(new InfoPage(1,1,1));

    List<Consulta> ls = new ArrayList();
    ls.add(consulta);

    when(consultaDAO.getConsultaByDNI(any(), any()))
      .thenReturn(ls);

    mockMvc
      .perform(get("/consultas?pageNumber=123&pageSize=128"))
      .andExpect(status().isOk());
  }

  @Test
  public void getConsulta_andReturnThem() throws Exception{
    Consulta consulta = new Consulta(1234, Date.valueOf("2021-03-03"), Date.valueOf("2021-04-01"), 
    "Enfermedad", "Medicamento", "Pruebas Laboratorio", "Observaciones", "11111111V", "NombreDoctor", 
    "ApellidosDoctor", Date.valueOf("1980-01-01") , "correodoctor@gotvg.com", "Masculino", "0412", 
    "00000000F", "NombrePaciente",  "ApellidoPaciente", Date.valueOf("1990-01-01") , 
    "correopaciente@gotvg.com", "Masculino");

    List<Consulta> ls = new ArrayList();
    ls.add(consulta);

    when(consultaDAO.getMetadataFromQuery(any(), anyString()))
      .thenReturn(new InfoPage());

    when(consultaDAO.getConsultaByDNI(any(), any()))
      .thenReturn(ls);

    mockMvc
      .perform(get("/consultasDNI/dni/pagina"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$[*]", hasSize(2)))
      .andExpect(jsonPath("$.consulta", hasSize(1)))
      .andExpect(jsonPath("$.consulta[0].idConsulta", is(consulta.getIdConsulta())))
      .andExpect(jsonPath("$.consulta[0].fechaRegistroPaciente", is(consulta.getFechaRegistroPaciente().toString())))
      .andExpect(jsonPath("$.consulta[0].fechaConsulta", is(consulta.getFechaConsulta().toString())))
      .andExpect(jsonPath("$.consulta[0].enfermedadesDiagnosticadas", is(consulta.getEnfermedadesDiagnosticadas())))
      .andExpect(jsonPath("$.consulta[0].medicamentosRecetados", is(consulta.getMedicamentosRecetados())))
      .andExpect(jsonPath("$.consulta[0].pruebasDeLaboratorioARealizar", is(consulta.getPruebasDeLaboratorioARealizar())))
      .andExpect(jsonPath("$.consulta[0].observaciones", is(consulta.getObservaciones())))
      .andExpect(jsonPath("$.consulta[0].dniDoctor", is(consulta.getDniDoctor())))
      .andExpect(jsonPath("$.consulta[0].nombreDoctor", is(consulta.getNombreDoctor())))
      .andExpect(jsonPath("$.consulta[0].apellidosDoctor", is(consulta.getApellidosDoctor())))
      .andExpect(jsonPath("$.consulta[0].fechaNacimientoDoctor", is(consulta.getFechaNacimientoDoctor().toString())))
      .andExpect(jsonPath("$.consulta[0].correoElectronicoDoctor", is(consulta.getCorreoElectronicoDoctor())))
      .andExpect(jsonPath("$.consulta[0].generoDoctor", is(consulta.getGeneroDoctor())))
      .andExpect(jsonPath("$.consulta[0].passwordDoctor", is(consulta.getPasswordDoctor())))
      .andExpect(jsonPath("$.consulta[0].dniPaciente", is(consulta.getDniPaciente())))
      .andExpect(jsonPath("$.consulta[0].nombrePaciente", is(consulta.getNombrePaciente())))
      .andExpect(jsonPath("$.consulta[0].apellidosPaciente", is(consulta.getApellidosPaciente())))
      .andExpect(jsonPath("$.consulta[0].correoElectronicoPaciente", is(consulta.getCorreoElectronicoPaciente())))
      .andExpect(jsonPath("$.consulta[0].fechaNacimientoPaciente", is(consulta.getFechaNacimientoPaciente().toString())))
      .andExpect(jsonPath("$.consulta[0].generoPaciente", is(consulta.getGeneroPaciente())));
  }

  @Test
  public void getConsulta_dniLongitudMala() throws Exception{
    mockMvc
      .perform(get(String.format("/consultasDNI/%s/pagina?pageNumber=1&pageSize=1","123456789P")))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void getConsulta_dniNoExiste() throws Exception{
    mockMvc
      .perform(get(String.format("/consultasDNI/%s/pagina?pageNumber=1&pageSize=1","12345678X")))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void addConsulta_whenWorks() throws Exception {
    Consulta request = new Consulta(1234, Date.valueOf("2021-03-03"), Date.valueOf("2021-04-01"), "Enfermedad", "Medicamento", "Pruebas Laboratorio", "Observaciones", "11111111V", "NombreDoctor", "ApellidosDoctor", Date.valueOf("1980-01-01") , "correodoctor@gotvg.com", "Masculino", "0412", "00000000F", "NombrePaciente",  "ApellidoPaciente", Date.valueOf("1990-01-01") , "correopaciente@gotvg.com", "Masculino");

    mockMvc.perform(post("/addConsulta")
      .content(new JSONObject(request).toString())
      .contentType(MediaType.APPLICATION_JSON))
    .andExpect(status().isOk())
    .andExpect(content().string(""));
  }
/*
  @Test
  public void eliminarConsulta_llamarConConsultaInexistente()
    throws Exception {
    when(consultaDAO.eliminarConsultaByID(anyInt())).thenReturn(false);

    mockMvc.perform(post("/eliminarConsulta/4")).andExpect(status().isNotFound());
  }

  @Test
  public void eliminarConsulta_malURL()
    throws Exception {
    mockMvc.perform(post("/eliminarConsulta")).andExpect(status().isBadRequest());
  }

  @Test
  public void eliminarConsulta_whenWorks() throws Exception {
    Integer request = 40;

    

    mockMvc.perform(post("/eliminarConsulta")
    .param("idConsulta", "40")).andReturn().getResponse().getStatus();
    //.andExpect(status().isOk())
    //.andExpect(content().string("40"));
  }
*/
}